import React from 'react';

class CustomTextInput extends React.Component {
    constructor(props) {
        super(props);
        
        this.textInput = null;
        this.state = {nome: ""};
       
        this.handleChange = this.handleChange.bind(this);
        this.setTextInputRef = this.setTextInputRef.bind(this);
        this.focusTextInput = this.focusTextInput.bind(this);
    }

    setTextInputRef(element) {
        this.textInput = element;
    }
    
    focusTextInput() {
        // Focus the text input using the raw DOM API
        if (this.textInput) {
            this.textInput.focus();
        }
    }
    
    handleChange(e) {
        const {value:newNome} = e.target;
        if (newNome.length < 14) {
            this.setState({nome: e.target.value});
        }
        console.log(e, this.state);       
    }
    
    render() {
        // Use the `ref` callback to store a reference to the text input DOM
        // element in an instance field (for example, this.textInput).
        const {nome} = this.state;  
        return (
            <div>
                <input
                    type="text"
                    ref={this.setTextInputRef}
                    onFocus={() => console.log(this.textInput.value)}
                    onChange={this.handleChange}  
                    value={nome}
                />
                <input
                    type="button"
                    value="Focus the text input"
                    onClick={this.focusTextInput}
                />
            </div>
        );
    }   
}
    
export default CustomTextInput;