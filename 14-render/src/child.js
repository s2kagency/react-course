import React from 'react';

class Child extends React.Component {
    constructor(props) {
        super(props);
        console.log("Child - constructor");
    }

    componentWillMount() {
        console.log("Child - ComponentWillMount");
    }

    componentDidMount() {
        console.log("Child - ComponentDidMount");
    }

    componentWillReceiveProps(nextProps) {
        console.log("Child - componentWillReceiveProps");
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log("Child - ShouldComponentUpdate");
        return true;
    }

    componentWillUpdate(nextProps, nextState) {
        console.log("Child - componentWillUpdate");
    }

    componentDidUpdate(nextProps, nextState) {
        console.log("Child - componentDidUpdate");
    }

    componentWillUnmount() {
        console.log("Child - componentWillUnmount");
    }

    render() {
        const {counter, children} = this.props;
        console.log("Child - Render");
        return(
            <div>
                <h1>Total: {counter}</h1>
                {children}
            </div>
        );
    }
}
export default Child;