import React from 'react';
import ReactDOM from 'react-dom';
import AutoFocusTextInput from './components/auto-focus-text-input';

function App () {
    return(
        <div>
            <AutoFocusTextInput />
            <AutoFocusTextInput />
            <AutoFocusTextInput />
            <AutoFocusTextInput />
            <AutoFocusTextInput />
        </div>
    );
}
    
ReactDOM.render(
    <App />,
    document.getElementById('root')
);