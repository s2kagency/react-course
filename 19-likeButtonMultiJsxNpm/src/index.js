class LikeButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { liked: false };
    this.like = this.like.bind(this);
    console.log(this.props);
  }

  like() {
    console.log(this);
    this.setState({ liked: true });
    this.props.increaseCounter(this.props.articleId);
  }

  render() {
    if (this.state.liked) {
      return 'You liked comment number ' + this.props.articleId;
    }

    // Mostra un <button> "Mi Piace"
    return (
      <button onClick={this.like}>
        Mi Piace
      </button>
    );
  }
}

class LikeButtons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      likesCounter: 0
    };
    this.incrementCounterHandler = this.incrementCounterHandler.bind(this);
  }

  incrementCounterHandler() {
    this.setState({
      likesCounter: this.state.likesCounter + 1
    })
  }

  render() {
    const {likesCounter} = this.state;
    let likeButtons = [];
    for (let i = 0; i < this.props.numberOfItems; i ++) {
      likeButtons.push(
        <LikeButton 
          articleId={i} 
          key={`like_button_${i}`} 
          increaseCounter={this.incrementCounterHandler} 
        />
      )
    }
    return(<React.Fragment>
      <p>{likesCounter}</p>
      {/* <LikeButton articleId={0} /> */}
      {/* {this.props.items} */}
      {likeButtons}
    </React.Fragment>);
  }
}

class Feed extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      likesCounter: this.props.items.reduce((aggregate, item) => {
        aggregate[item.id] = item.likes;
        return aggregate;
      }, []),
      updatedLikesCounter: this.props.items.reduce((sum, item) => {
        return sum + item.likes;
      }, 0)
    };
    this.incrementLikesCounterHandler = this.incrementLikesCounterHandler.bind(this);

    console.log('state', this.state);
  }

  incrementLikesCounterHandler(itemId) {
    let likesCounter = {
      ...this.state.likesCounter,
      [itemId]: this.state.likesCounter[itemId] + 1
    };
    
    let updatedLikesCounter = Object.values(likesCounter).reduce((sum, item) => {
      return sum + item;
    }, 0);
    
    this.setState({
      likesCounter: likesCounter,
      updatedLikesCounter: updatedLikesCounter
    });
  }

  render() {

    console.log('state', this.state);

    const {likesCounter} = this.state;
    // let likeButtons = [];
    // for (let i = 0; i < this.props.numberOfItems; i ++) {
    //   likeButtons.push(
    //     <LikeButton 
    //       articleId={i} 
    //       key={`like_button_${i}`} 
    //       increaseCounter={this.incrementLikesCounterHandler} 
    //     />
    //   )
    // }
    return(<React.Fragment>
      <h1>React Magazine</h1>
      <span>{this.state.updatedLikesCounter}</span>
      {this.props.items.map((item) => {
        return (
          <div className="feed-article" key={`feedArticle_${item.id}`}>
            <h3>{item.title}</h3>
            <p>{item.content}</p>
            <LikeButton 
              articleId={item.id}
              increaseCounter={this.incrementLikesCounterHandler} 
            />
          </div>
        );
      })}
      {/* <p>{likesCounter}</p> */}
      {/* <LikeButton articleId={0} /> */}
      {/* {this.props.items} */}
      {/* {likeButtons} */}
    </React.Fragment>);
  }
}


// Find all DOM containers, and render Like buttons into them.
const domContainer = document.getElementById('feed_wrapper');
const items = [
  {
    id: 1,
    title: 'What is Lorem Ipsum?',
    content: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`,
    author: 'Pippo Verdi',
    comments: [
      {
        content: 'commento',
        author: 'Luca Fregoso'
      },
      {
        content: 'commento 2',
        author: 'Luca Fregoso'
      },
      {
        content: 'commento 3',
        author: 'Luca Fregoso'
      },
    ],
    likes: 11
  },
  {
    id: 2,
    title: 'Why do we use it?',
    content: `It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).`,
    author: 'Valeria Rossi',
    comments: [
      {
        content: 'commento',
        author: 'Lucia Rossi'
      },
      {
        content: 'commento 2',
        author: 'Carlo Blu'
      },
    ],
    likes: 102
  },
];

ReactDOM.render(<Feed items={items}/>, domContainer);