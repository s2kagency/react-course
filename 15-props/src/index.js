import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

class Ciao extends React.Component {
    constructor (props) {
        super(props);
        console.log({...this.props});
    }
    
    render() {
        const {nome} = this.props
        return <h1>Ciao, {nome}</h1>;
    }
}

Ciao.propTypes = {
    nome: PropTypes.string
};

// Specifies the default values for props:
Ciao.defaultProps = {
    nome: 'Ignoto'
};

function App() {
    return (
        <div>
            <Ciao nome="Sara" />
            <Ciao nome="Cahal" />
            <Ciao nome="Edite" />
        </div>
    );
}

ReactDOM.render(
    <App />,
    document.getElementById('root')
);