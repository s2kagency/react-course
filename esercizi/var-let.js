var functions = [];
for (var iterator = 0; iterator < 10; iterator ++) {
    functions.push(function() {
        console.log(iterator);
    });
}

functions.forEach(function(func) {
    func();
});

let functions = [];
for (let iterator = 0; iterator < 10; iterator ++) {
    functions.push(function() {
        console.log(iterator);
    });
}

functions.forEach(function(func) {
    func();
});

