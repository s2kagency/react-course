// === Arrays

var [a, b] = [1, 2];
console.log(a, b);
//=> 1 2


// Destruttura il risultato di una funzione e selezionane solo una parte
var foo = () => [1, 2, 3];

var [a, b] = foo();
console.log(a, b);
// => 1 2


// Ometti dei valori
var [a, , b] = [1, 2, 3];
console.log(a, b);
// => 1 3


// Combina con spread/rest
var [a, ...b] = [1, 2, 3];
console.log(a, b);
// => 1 [ 2, 3 ]


// Fail-safe.
var [, , , a, b] = [1, 2, 3];
console.log(a, b);
// => undefined undefined

// Fail-safe.
var [, , , a, b] = [1, 2, 3, 4, 5];
console.log(a, b);
// => 4, 5


// Inverti due variabili senza passare da temporanee
var a = 1, b = 2;
[b, a] = [a, b];
console.log(a, b);
// => 2 1


// Destruttura array anche in profondità
var [a, [b, [c, d]]] = [1, [2, [[[3, 4], 5], 6]]];
console.log({a}, {b}, {c}, {d});
// => a: 1 b: 2 c: [ [ 3, 4 ], 5 ] d: 6


// === Oggetti

var {user: x} = {user: 5};
console.log(x);
// => 5


// Fail-safe
var {user: x} = {user2: 5};
console.log(x);
// => undefined


// Assegna ad attributi diversi
var {prop: x, prop2: y} = {prop: 5, prop2: 10};
console.log(x, y);
// => 5 10

// Short-hand syntax
var { prop, prop2} = {prop: 5, prop2: 10};
console.log(prop, prop2);
// => 5 10

// Identico a:
var { prop: prop, prop2: prop2} = {prop: 5, prop2: 10};
console.log(prop, prop2);
// => 5 10

// === Destrutturazione combinata di oggetti e array

var {prop: x, prop2: [, y]} = {prop: 5, prop2: [10, 100]};
console.log(x, y);
// => 5 100


// === Destrutturazione profonda

var {
  prop: x,
  prop2: {
    prop2: {
      nested: [ , , b]
    }
  }
} = { prop: "Hello", prop2: { prop2: { nested: ["a", "b", "c"]}}};
console.log(x, b);
// => Hello c


// Utile anche per i parametri delle funzioni
var foo = function ({prop: x}) {
  console.log(x);
};

foo({invalid: 1});
foo({prop: 1});
// => undefined
// => 1

// === Esempi avanzati
var foo = function ({
  prop: x,
  prop2: {
    prop2: {
      nested: b
    }
  }
}) {
  console.log(x, ...b);
};
foo({ prop: "Hello", prop2: { prop2: { nested: ["a", "b", "c"]}}});
// => Hello a b c


// === In combinazione con altre features ES6.

// Computed property names
const name = 'fieldName';
const computedObject = { [name]: name }; // { 'fieldName': 'fieldName' })
const { [name]: nameValue } = computedObject;
console.log(nameValue)
// => fieldName



// === Rest and defaults
var ajax = function ({ url = "localhost", port: p = 80}, ...data) {
  console.log("Url:", url, "Port:", p, "Rest:", data);
};

ajax({url: "www.google.it", port: 22}, "Google SSH", {"nome": "Luca", "cognome": "Fregoso"})

// => Url: www.google.it Port: 22 Rest: [ 'Google SSH', {...} ]

ajax({ }, "additional", "data", "hello");
// => Url: localhost Port: 80 Rest: [ 'additional', 'data', 'hello' ]

ajax({ });
// => Url: localhost Port: 80 Rest: []

// Non funziona perché la destrutturazione non trova nulla
ajax();
//  => Uncaught TypeError: Cannot match against 'undefined' or 'null'

// Se vogliamo permettere la chiamata senza parametri dobbiamo impostare default per tutto.
var ajax = ({ url: url = "localhost", port: p = 80} = {}) => {
  console.log("Url:", url, "Port:", p);
};

// Ora funziona
ajax();
// => Url: localhost Port: 80

ajax({ });
// => Url: localhost Port: 80

ajax({ port: 8080 });
//  => Url: localhost Port: 8080

ajax({ url: "someHost", port: 8080 });
//  => Url: someHost Port: 8080


// === Come se usassimo _.pluck
var users = [
  { user: "Name1" },
  { user: "Name2" },
  { user: "Name2" },
  { user: "Name3" }
];
var names = users.map( ({ user }) => user );
console.log(names);
// => [ 'Name1', 'Name2', 'Name2', 'Name3' ]


// === Anche in loop for..of
var users = [
  { user: "Name1" },
  { user: "Name2", age: 2 },
  { user: "Name2" },
  { user: "Name3", age: 4 }
];

for (let { user, age = "DEFAULT AGE" } of users) {
  console.log(user, age);
}
// => Name1 DEFAULT AGE
// => Name2 2
// => Name2 DEFAULT AGE
// => Name3 4