var employee = { 
  details: function (company, skills) {
    return `${this.name} (${this.age} anni) lavora in ${company} su ${skills}`;
  }
}
var stud1 = { 
  name:"Luca", 
  age: "35", 
} 
var stud2 = { 
  name:"Simone", 
  age: "34", 
} 

console.log(employee.details.apply(stud1, ["HRM", "PHP, JavaScript, MySQL"]));
console.log(employee.details.apply(stud2, ["HRM", "PHP, JavaScript, Ruby, Mongo"]));