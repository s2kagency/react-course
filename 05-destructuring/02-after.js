const persona = {
    nome: "Luca",
    cognome: "Fregoso",
    stato: "Italia",
    citta: "La Spezia",
    twitter: "scakko"
}

const { nome, twitter } = persona;

console.log(`${nome} [@${twitter}] `)