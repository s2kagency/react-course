const persona = {
    nome: "Luca",
    cognome: "Fregoso",
    stato: "Italia",
    citta: "La Spezia",
    twitter: "scakko"
}

const { nome: n, twitter: t } = persona;

console.log(`${n} [@${t}] `)