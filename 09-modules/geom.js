export const PI = 3.14;

export function area(r){
    return r * r * PI;
}

export function circ(r){
    return 2 * r * PI;
}