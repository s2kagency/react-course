const rand = Math.floor(Math.random() * 10)
let message

if (rand % 2) {
    message = 'dispari'
} else {
    message = 'pari'
}

console.log(`Il numero random ${rand} è ${message}`)