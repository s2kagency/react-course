
// Constructing
// one property at a time
const jane1 = {};
jane1.first = 'Jane';
jane1.last = 'Doe';

// multiple properties
const jane2 = {
  first: 'Jane',
  last: 'Doe',
};

// Extracting
// one property at a time
const f1 = jane.first;
const l1 = jane.last;

// multiple properties
const {first: f2, last: l2} = jane;

// Object destructuring
const address = {
  street: 'Evergreen Terrace',
  number: '742',
  city: 'Springfield',
  state: 'NT',
  zip: '49007',
};

const { street: s, city: c } = address;

// shorthands
const { street, city } = address;

// Rest
const obj = { a: 1, b: 2, c: 3 };
const { a: propValue, ...remaining } = obj;

// Array destructuring
const [x, y] = ['a', 'b'];
const [, x, y] = ['a', 'b', 'c'];

// Swapping variable values
let x = 'a';
let y = 'b';

// Default value
const {prop: p = 123} = {}; // p = 123

// Nested destructuring
const arr = [
  { first: 'Jane', last: 'Bond' },
  { first: 'Lars', last: 'Croft' },
];
const [, {first}] = arr; // first = arr[1].first

[x,y] = [y,x];