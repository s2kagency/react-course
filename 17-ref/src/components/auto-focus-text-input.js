import React from 'react';
import CustomTextInput from './custom-text-input';

class AutoFocusTextInput extends React.Component {
    constructor(props) {
        super(props);
        this.textInput = React.createRef();
    }
    
    render() {
        return (
            <CustomTextInput ref={this.textInput} />
        );
    }
}

export default AutoFocusTextInput;
