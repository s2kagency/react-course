import React from 'react';
import ReactDOM from 'react-dom';
import Child from './child';
import IncrementButton from './increment-button';

class App extends React.Component {
    constructor(props) {
        console.log("App - constructor");
        super(props);
        // counter e child
        this.state = {counter: 0, child: true};

        // This binding is necessary to make `this` work in the callback
        this.incrementCounter = this.incrementCounter.bind(this);
    }
    
    componentWillMount() {
        console.log("App - ComponentWillMount");
        this.setState({counter: 1});
    }
    
    componentDidMount() {
        console.log("App - ComponentDidMount");
    }
    
    shouldComponentUpdate(nextProps, nextState) {
        console.log("App - ShouldComponentUpdate");
        return true;
    }
    
    componentWillUpdate(nextProps, nextState) {
        console.log("App - componentWillUpdate");
    }
    
    componentDidUpdate(nextProps, nextState) {
        console.log("App - componentDidUpdate");
    }
    
    // aggiungiamo un metodo unmount()
    unmount() {
        const {child:isMounted} = this.state;
        if (isMounted) {
            this.setState({child: false});
        }
    }
    
    // aggiungiamo un metodo mount()
    mount() {
        const {child:isMounted} = this.state;
        if (!isMounted) {
            this.setState({child: true});
        }
    }
    
    increment() {
        this.setState((prevState, props) => ({
            counter: prevState.counter + 1
        }));
    }
    
    incrementCounter(value) {
        this.setState((prevState, props) => ({
            counter: prevState.counter + parseInt(value, 10)
        }));
    }
    
    render() {
        console.log("App - Render");
        
        const {child:childEnabled, counter} = this.state;

        const numbers = [1, 2, 3, 4, 5, 6, 7, 8];
        const listItems = numbers.map((number) =>
            <IncrementButton onClick={this.incrementCounter} increment={number} key={`increment_${number}`} counter={counter}/>
        );
        
        return(
            <div>
                {childEnabled && (<Child counter={counter}>
                    {listItems}
                </Child>)}
                <button type="button" onClick={() => this.unmount()}>Unmount Child Component</button>
                <button type="button" onClick={() => this.mount()}>Mount Child Component</button>
            </div>
        );
    }
}
    
ReactDOM.render(
    <App />,
    document.getElementById('root')
);