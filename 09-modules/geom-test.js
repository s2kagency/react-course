import { area, circ } from './geom';
import inquirer from 'inquirer';

const questions = [{
  type: 'input',
  name: 'raggio',
  message: "Quanto è grande il raggio?",
}]

// let raggio = prompt("Quanto è grande il raggio?");

//Calcolo l'area

inquirer.prompt(questions).then(answers => {
  // console.log(`Hi ${answers['name']}!`)
    //Calcolo l'area
  console.log( area(answers['raggio']) );

  //Calcolo la circonferenza
  console.log( circ(answers['raggio']) );
})

// let raggio = prompt("Quanto è grande il raggio?");

