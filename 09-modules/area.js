const PI = 3.14;

export default function(r){
    return r * r * PI;
}