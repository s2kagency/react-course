import React from 'react';
import PropTypes from 'prop-types';

class IncrementButton extends React.Component {
    constructor(props) {
        super(props);
        console.log("Child - constructor");
    }

    render() {
        console.log("Child - Render");
        const {counter, increment, onClick} = this.props;
        return(
            <button type="button" onClick={() => onClick(increment)}>Incrementa {counter} di {increment}</button>
        );
    }
}

// Specifies the prop types:
IncrementButton.propTypes = {
    onClick: PropTypes.func,
    increment: PropTypes.number,
    counter: PropTypes.number
};

// Specifies the default values for props:
IncrementButton.defaultProps = {
    onClick: () => {},
    increment: 0,
    counter: 0
};

export default IncrementButton;