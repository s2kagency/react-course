const persona = {
    nome: "Luca",
    cognome: "Fregoso",
    stato: "Italia",
    citta: "La Spezia",
    twitter: "scakko"
}

const nome = persona.nome;
const twitter = persona.twitter;

console.log(`${nome} [@${twitter}] `)