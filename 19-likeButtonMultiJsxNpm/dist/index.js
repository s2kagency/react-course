var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LikeButton = function (_React$Component) {
  _inherits(LikeButton, _React$Component);

  function LikeButton(props) {
    _classCallCheck(this, LikeButton);

    var _this = _possibleConstructorReturn(this, (LikeButton.__proto__ || Object.getPrototypeOf(LikeButton)).call(this, props));

    _this.state = { liked: false };
    _this.like = _this.like.bind(_this);
    console.log(_this.props);
    return _this;
  }

  _createClass(LikeButton, [{
    key: 'like',
    value: function like() {
      console.log(this);
      this.setState({ liked: true });
      this.props.increaseCounter(this.props.articleId);
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.state.liked) {
        return 'You liked comment number ' + this.props.articleId;
      }

      // Mostra un <button> "Mi Piace"
      return React.createElement(
        'button',
        { onClick: this.like },
        'Mi Piace'
      );
    }
  }]);

  return LikeButton;
}(React.Component);

var LikeButtons = function (_React$Component2) {
  _inherits(LikeButtons, _React$Component2);

  function LikeButtons(props) {
    _classCallCheck(this, LikeButtons);

    var _this2 = _possibleConstructorReturn(this, (LikeButtons.__proto__ || Object.getPrototypeOf(LikeButtons)).call(this, props));

    _this2.state = {
      likesCounter: 0
    };
    _this2.incrementCounterHandler = _this2.incrementCounterHandler.bind(_this2);
    return _this2;
  }

  _createClass(LikeButtons, [{
    key: 'incrementCounterHandler',
    value: function incrementCounterHandler() {
      this.setState({
        likesCounter: this.state.likesCounter + 1
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var likesCounter = this.state.likesCounter;

      var likeButtons = [];
      for (var i = 0; i < this.props.numberOfItems; i++) {
        likeButtons.push(React.createElement(LikeButton, {
          articleId: i,
          key: 'like_button_' + i,
          increaseCounter: this.incrementCounterHandler
        }));
      }
      return React.createElement(
        React.Fragment,
        null,
        React.createElement(
          'p',
          null,
          likesCounter
        ),
        likeButtons
      );
    }
  }]);

  return LikeButtons;
}(React.Component);

var Feed = function (_React$Component3) {
  _inherits(Feed, _React$Component3);

  function Feed(props) {
    _classCallCheck(this, Feed);

    var _this3 = _possibleConstructorReturn(this, (Feed.__proto__ || Object.getPrototypeOf(Feed)).call(this, props));

    _this3.state = {
      likesCounter: _this3.props.items.reduce(function (aggregate, item) {
        aggregate[item.id] = item.likes;
        return aggregate;
      }, []),
      updatedLikesCounter: _this3.props.items.reduce(function (sum, item) {
        return sum + item.likes;
      }, 0)
    };
    _this3.incrementLikesCounterHandler = _this3.incrementLikesCounterHandler.bind(_this3);

    console.log('state', _this3.state);
    return _this3;
  }

  _createClass(Feed, [{
    key: 'incrementLikesCounterHandler',
    value: function incrementLikesCounterHandler(itemId) {
      var likesCounter = Object.assign({}, this.state.likesCounter, _defineProperty({}, itemId, this.state.likesCounter[itemId] + 1));

      var updatedLikesCounter = Object.values(likesCounter).reduce(function (sum, item) {
        return sum + item;
      }, 0);

      this.setState({
        likesCounter: likesCounter,
        updatedLikesCounter: updatedLikesCounter
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      console.log('state', this.state);

      var likesCounter = this.state.likesCounter;
      // let likeButtons = [];
      // for (let i = 0; i < this.props.numberOfItems; i ++) {
      //   likeButtons.push(
      //     <LikeButton 
      //       articleId={i} 
      //       key={`like_button_${i}`} 
      //       increaseCounter={this.incrementLikesCounterHandler} 
      //     />
      //   )
      // }

      return React.createElement(
        React.Fragment,
        null,
        React.createElement(
          'h1',
          null,
          'React Magazine'
        ),
        React.createElement(
          'span',
          null,
          this.state.updatedLikesCounter
        ),
        this.props.items.map(function (item) {
          return React.createElement(
            'div',
            { className: 'feed-article', key: 'feedArticle_' + item.id },
            React.createElement(
              'h3',
              null,
              item.title
            ),
            React.createElement(
              'p',
              null,
              item.content
            ),
            React.createElement(LikeButton, {
              articleId: item.id,
              increaseCounter: _this4.incrementLikesCounterHandler
            })
          );
        })
      );
    }
  }]);

  return Feed;
}(React.Component);

// Find all DOM containers, and render Like buttons into them.


var domContainer = document.getElementById('feed_wrapper');
var items = [{
  id: 1,
  title: 'What is Lorem Ipsum?',
  content: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
  author: 'Pippo Verdi',
  comments: [{
    content: 'commento',
    author: 'Luca Fregoso'
  }, {
    content: 'commento 2',
    author: 'Luca Fregoso'
  }, {
    content: 'commento 3',
    author: 'Luca Fregoso'
  }],
  likes: 11
}, {
  id: 2,
  title: 'Why do we use it?',
  content: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).',
  author: 'Valeria Rossi',
  comments: [{
    content: 'commento',
    author: 'Lucia Rossi'
  }, {
    content: 'commento 2',
    author: 'Carlo Blu'
  }],
  likes: 102
}];

ReactDOM.render(React.createElement(Feed, { items: items }), domContainer);